class Articles {
  /**
     * les articles
     * la description
     * Lien vers l'article (href)
     */
  constructor (nomArticle, description, href) {
    this.nomArticle = nomArticle
    this.description = description
    this.href = href
  }
}
const blog = [
  new Articles('Blog 1', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 2', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 3', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 4', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 5', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 6', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 7', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 8', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 9', 'Description de cet article :', 'Cliquez ici pour en savoir plus !'),
  new Articles('Blog 10', 'Description de cet article :', 'Cliquez ici pour en savoir plus !')
]
exports.mesBlog = blog
