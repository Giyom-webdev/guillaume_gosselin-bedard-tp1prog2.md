class Formations {
  /**
     * les formations
     * une courte description
     * Lien vers l'article
     */
  constructor (nomFormations, description) {
    this.nomFormations = nomFormations
    this.description = description
  }
}

const formations = [
  new Formations('Formation 1', 'Description:'),
  new Formations('Formation 2', 'Description:'),
  new Formations('Formation 3', 'Description:'),
  new Formations('Formation 4', 'Description:'),
  new Formations('Formation 5', 'Description:')
]

exports.mesFormations = formations
