const express = require('express')
const router = express.Router()
const formations = require('../module/formation')
const blog = require('../module/blog')
const { mesBlog } = require('../module/blog')
const { mesFormations } = require('../module/formation')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Accueil' })
})

router.get('/formations', function (req, res, next) {
  res.render('formations', { title: 'Nos formations', formations: formations.mesFormations })
})

router.get('/web', function (req, res, next) {
  res.render('web', { title: 'Page MongoDB' })
})

router.get('/blog', function (req, res, next) {
  res.render('blog', { title: 'Notre Blog', blog: blog.mesBlog })
})
router.get('/nodejs', function (req, res, next) {
  res.render('nodejs', { title: 'Page Nodejs' })
})

router.get('/contact', function (req, res, next) {
  res.render('contact', { title: 'Vos Informations, S.V.P' })
})

module.exports = router
